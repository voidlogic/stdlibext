package compressmisc

import (
	"bytes"
	"compress/gzip"
	"compress/zlib"
)

func CompressGzipFastest(data []byte) []byte {
	buffer := new(bytes.Buffer)
	zipper, _ := gzip.NewWriterLevel(buffer, gzip.BestSpeed)
	zipper.Write(data)
	zipper.Close()
	return buffer.Bytes()
}

func CompressGzip(data []byte) []byte {
	buffer := new(bytes.Buffer)
	zipper, _ := gzip.NewWriterLevel(buffer, gzip.DefaultCompression)
	zipper.Write(data)
	zipper.Close()
	return buffer.Bytes()
}

func CompressGzipSmallest(data []byte) []byte {
	buffer := new(bytes.Buffer)
	zipper, _ := gzip.NewWriterLevel(buffer, gzip.BestCompression)
	zipper.Write(data)
	zipper.Close()
	return buffer.Bytes()
}

func CompressZlibFastest(data []byte) []byte {
	buffer := new(bytes.Buffer)
	zipper, _ := zlib.NewWriterLevel(buffer, zlib.BestSpeed)
	zipper.Write(data)
	zipper.Close()
	return buffer.Bytes()
}

func CompressZlib(data []byte) []byte {
	buffer := new(bytes.Buffer)
	zipper, _ := zlib.NewWriterLevel(buffer, zlib.DefaultCompression)
	zipper.Write(data)
	zipper.Close()
	return buffer.Bytes()
}

func CompressZlibSmallest(data []byte) []byte {
	buffer := new(bytes.Buffer)
	zipper, _ := zlib.NewWriterLevel(buffer, zlib.BestCompression)
	zipper.Write(data)
	zipper.Close()
	return buffer.Bytes()
}
