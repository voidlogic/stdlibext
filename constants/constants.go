package constants

import "fmt"

const (
	BYTE = 1
	KB   = BYTE << 10
	MB   = KB << 10
	GB   = MB << 10
	TB   = GB << 10
	PB   = TB << 10
)

func ByteUnitString(size int) string {
	var display string
	if size/PB > 0 {
		display = fmt.Sprintf("%.2f PB", float64(size)/float64(PB))
	} else if size/TB > 0 {
		display = fmt.Sprintf("%.2f TB", float64(size)/float64(TB))
	} else if size/GB > 0 {
		display = fmt.Sprintf("%.2f GB", float64(size)/float64(GB))
	} else if size/MB > 0 {
		display = fmt.Sprintf("%.2f MB", float64(size)/float64(MB))
	} else if size/KB > 0 {
		display = fmt.Sprintf("%.2f KB", float64(size)/float64(KB))
	} else {
		display = fmt.Sprintf("%d bytes", size)
	}
	return display
}
