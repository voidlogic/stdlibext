package events

import (
	"os"
	"os/signal"
	"sync"
	"syscall"
)

//signal, err or both may be nil
type ShutdownHandler func(signal os.Signal, err error)

var (
	defShutdownSigs                   = []os.Signal{syscall.SIGTERM, syscall.SIGHUP, syscall.SIGINT, syscall.SIGQUIT}
	shutdownFunc      ShutdownHandler = wrapExclusiveShutdown(DefaultShutdownHandler)
	shutdownStateLock sync.Mutex      //Protects shutdownFunc reference state
	shutdownLock      sync.Mutex      //Ensures exclusive execution of shutdownFunc
)

func setupShutdownHandler() {
	shutdownStateLock.Lock()
	defer shutdownStateLock.Unlock()
	logger("Listening for: SIGTERM, SIGHUP, SIGINT and SIGQUIT signals.")
	signals := make(chan os.Signal, 1)
	signal.Notify(signals, defShutdownSigs...)
	go signalListenWorker(signals)
}

func SetShutdownHandler(handler ShutdownHandler) {
	shutdownStateLock.Lock()
	defer shutdownStateLock.Unlock()

	shutdownFunc = wrapExclusiveShutdown(handler)
}

func RunShutdownHandler(err error) {
	shutdownStateLock.Lock()
	defer shutdownStateLock.Unlock()
	if shutdownFunc != nil {
		shutdownFunc(nil, err)
	}
}

func signalListenWorker(signals chan os.Signal) {
	for {
		signal := <-signals
		shutdownStateLock.Lock()
		defer shutdownStateLock.Unlock()
		if shutdownFunc != nil {
			shutdownFunc(signal, nil)
		}
	}
}

func wrapExclusiveShutdown(handler ShutdownHandler) ShutdownHandler {
	if handler != nil {
		return func(signal os.Signal, err error) {
			shutdownLock.Lock()
			defer shutdownLock.Unlock()

			handler(signal, err)
		}
	}
	return func(signal os.Signal, err error) {
	}
}

func DefaultShutdownHandler(signal os.Signal, err error) {
	var exitStatus = 0
	if signal != nil {
		logger("Caught signal: %v.", signal)
	} else if err != nil {
		logger("Fatal error: %s", err)
		exitStatus = 1
	}
	logger("Shutdown requested, will now terminate.")
	os.Exit(exitStatus)
}
