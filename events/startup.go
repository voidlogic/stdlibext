package events

import (
	"runtime"
)

type LogPrinter func(format string, v ...interface{})

var logger LogPrinter

func Setup(logPrinter LogPrinter) {
	logger = logPrinter
	setupMultithreading()
	setupShutdownHandler()
}

func setupMultithreading() {
	logicalCores := runtime.NumCPU()
	runtime.GOMAXPROCS(logicalCores)
	logger("Runtime goroutine thread-pool is set to %d threads.", logicalCores)
}
