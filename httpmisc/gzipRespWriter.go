package httpmisc

import (
	"compress/gzip"
	"io"
	"net/http"
	"strings"
)

const _CONTENT_SNIFF_MAX = 512

type gzipRespWriter struct {
	io.Writer
	http.ResponseWriter
	sniffed bool
}

func (this *gzipRespWriter) Write(content []byte) (int, error) {
	if !this.sniffed {
		if len(this.Header().Get("Content-Type")) == 0 {
			sniffLen := len(content)
			if sniffLen > _CONTENT_SNIFF_MAX {
				sniffLen = _CONTENT_SNIFF_MAX
			}
			this.Header().Set("Content-Type", http.DetectContentType(content[:sniffLen]))
		}
		this.sniffed = true
	}
	return this.Writer.Write(content)
}

func WrapGzipHandler(handler http.Handler) http.Handler {
	return http.HandlerFunc(func(writer http.ResponseWriter, resp *http.Request) {
		if !strings.Contains(resp.Header.Get("Accept-Encoding"), "gzip") {
			handler.ServeHTTP(writer, resp)
			return
		}
		writer.Header().Set("Content-Encoding", "gzip")
		gzipper, _ := gzip.NewWriterLevel(writer, gzip.BestSpeed)
		gzipWriter := &gzipRespWriter{
			Writer:         gzipper,
			ResponseWriter: writer,
		}
		defer gzipper.Close()
		handler.ServeHTTP(gzipWriter, resp)
	})
}

func WrapGzipHandlerFunc(handlerFunc http.HandlerFunc) http.Handler {
	return http.HandlerFunc(func(writer http.ResponseWriter, resp *http.Request) {
		if !strings.Contains(resp.Header.Get("Accept-Encoding"), "gzip") {
			handlerFunc(writer, resp)
			return
		}
		writer.Header().Set("Content-Encoding", "gzip")
		gzipper, _ := gzip.NewWriterLevel(writer, gzip.BestSpeed)
		gzipWriter := &gzipRespWriter{
			Writer:         gzipper,
			ResponseWriter: writer,
		}
		defer gzipper.Close()
		handlerFunc(gzipWriter, resp)
	})
}
