package pagecache

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"sync"
	"time"

	"util/errs"
	"util/logs"

	fsnotify "github.com/howeyc/fsnotify"
)

const (
	minCacheTTL = time.Second * 30
	maxCacheTTL = time.Hour * 72
	defCacheTTL = time.Hour * 4
)

type PageCache struct {
	lock        sync.RWMutex
	cache       map[string]*cacheEntry
	cacheHeader string
	watcher     *fsnotify.Watcher
	shutdownCh  chan bool
}

func NewPageCache(contentDir string) (*PageCache, error) {
	watcher, err := fsnotify.NewWatcher()
	if err != nil {
		return nil, errs.Append(err, "Could not create file system watcher")
	}
	if err = watchForEvents(watcher, contentDir); err != nil {
		return nil, errs.Append(err, "Could not configure file system watcher")
	}
	return &PageCache{
		cache:       make(map[string]*cacheEntry, 33),
		cacheHeader: fmt.Sprintf("public, max-age=%d", int(defCacheTTL.Seconds())),
		watcher:     watcher,
		shutdownCh:  make(chan bool),
	}, nil
}

func (this *PageCache) SetCacheTTL(ttl time.Duration) error {
	if ttl < minCacheTTL {
		return errs.New("Specified TTL: %s, is less than minimum allowed cache TTL: %s.", minCacheTTL, ttl)
	}
	if ttl > maxCacheTTL {
		return errs.New("Specified TTL: %s, is greater than maximum allowed cache TTL: %s.", maxCacheTTL, ttl)
	}
	this.cacheHeader = fmt.Sprintf("max-age=%d", int(ttl.Seconds()))
	return nil
}

func (this *PageCache) HandleRequest(handler http.Handler, req *http.Request, writer http.ResponseWriter) {
	this.lock.RLock()
	entry, isPresent := this.cache[req.URL.Path]
	this.lock.RUnlock()
	if !isPresent {
		entry = newCacheEntry(this.cacheHeader)
		req.Header.Del("If-Modified-Since") //We don't want to populate the cache with a 304 - Not Modified
		handler.ServeHTTP(entry, req)
		go func() {
			this.lock.Lock()
			this.cache[req.URL.Path] = entry
			this.lock.Unlock()
		}()
	}
	entry.ServeHTTP(writer, req)
}

func (this *PageCache) Close() error {
	this.shutdownCh <- true
	return nil
}

func (this *PageCache) fsEventWorker() {
	for {
		select {
		case <-this.watcher.Event:
			this.lock.Lock()
			for key, _ := range this.cache {
				delete(this.cache, key)
			}
			this.lock.Unlock()
		case err := <-this.watcher.Error:
			logs.LogErrorExp(err, "An error occured while watching for file system changes")
		case <-this.shutdownCh:
			this.watcher.Close()
			return
		}
	}
}

func watchForEvents(watcher *fsnotify.Watcher, directory string) error {
	const INVDATE_EVENT = fsnotify.FSN_MODIFY | fsnotify.FSN_DELETE | fsnotify.FSN_RENAME
	dirEntries, err := ioutil.ReadDir(directory)
	if err != nil {
		return errs.Append(err, "Could not read directory: %s", directory)
	} else if err = watcher.WatchFlags(directory, INVDATE_EVENT); err != nil {
		return errs.Append(err, "Could not watch directory for changes: %s", directory)
	}
	for _, fileInfo := range dirEntries {
		if fileInfo.IsDir() {
			subDir := fmt.Sprintf("%s%c%s", directory, os.PathSeparator, fileInfo.Name())
			if err = watchForEvents(watcher, subDir); err != nil {
				return errs.Append(err, "Could not watch sub-directory")
			}
		}
	}
	return nil
}
