package pagecache

import (
	"bytes"
	"net/http"
	"strconv"
	"strings"
	"time"

	"util/compressmisc"
)

type cacheEntry struct {
	headers                    http.Header
	buffer                     *bytes.Buffer
	plain, gzipped             []byte
	plainLenStr, gzippedLenStr string
	status                     int
	defCacheHeader             string
	lastUpdated                time.Time
	ttl                        time.Duration
}

func newCacheEntry(defCacheHeader string) *cacheEntry {
	return &cacheEntry{
		headers:        make(http.Header, 9),
		buffer:         new(bytes.Buffer),
		status:         -1,
		defCacheHeader: defCacheHeader,
	}
}

func (this *cacheEntry) ServeHTTP(writer http.ResponseWriter, req *http.Request) {
	//Make this cacheEntry ready to serve if it has not been served before
	if this.buffer != nil {
		this.finalizeContent()
	}
	headers := writer.Header()
	//Handle If-Modified-Since cache validation requests
	if serveIfBeforeStr := req.Header.Get("If-Modified-Since"); len(serveIfBeforeStr) > 0 {
		serveIfBefore, err := time.Parse(http.TimeFormat, serveIfBeforeStr)
		if err == nil && this.lastUpdated.Before(serveIfBefore.Add(1*time.Second)) {
			//The Date-Modified header truncates sub-second precision, so we will
			//use mtime < t+1s instead of mtime <= t to check for unmodified.
			delete(headers, "Content-Type")
			delete(headers, "Content-Length")
			writer.WriteHeader(http.StatusNotModified)
			return
		}
	}
	//Copy our headers into the http.ResponseWriter
	var key string
	var val []string
	for key, val = range this.headers {
		headers[key] = val
	}
	//Set Content-Encoding based on Accept-Encoding, Content-Length,
	//write response status and body
	if strings.Contains(req.Header.Get("Accept-Encoding"), "gzip") {
		headers.Set("Content-Encoding", "gzip")
		headers.Set("Content-Length", this.gzippedLenStr)
		writer.WriteHeader(this.status)
		writer.Write(this.gzipped)
	} else {
		headers.Set("Content-Length", this.plainLenStr)
		writer.WriteHeader(this.status)
		writer.Write(this.plain)
	}
}

func (this *cacheEntry) WriteHeader(status int) {
	this.status = status
}

func (this *cacheEntry) Header() http.Header {
	return this.headers
}

func (this *cacheEntry) Write(content []byte) (int, error) {
	this.buffer.Write(content)
	return len(content), nil
}

func (this *cacheEntry) finalizeContent() {
	content := this.buffer.Bytes()
	this.buffer = nil
	contentLen := len(content)
	//Set content type if needed
	if len(this.headers.Get("Content-Type")) == 0 {
		sniffLen := contentLen
		if sniffLen > _CONTENT_SNIFF_MAX {
			sniffLen = _CONTENT_SNIFF_MAX
		}
		this.headers.Set("Content-Type", http.DetectContentType(content[:sniffLen]))
	}
	//Set content
	this.plain, this.gzipped = content, compressmisc.CompressGzipSmallest(content)
	this.plainLenStr, this.gzippedLenStr = strconv.Itoa(contentLen), strconv.Itoa(len(this.gzipped))
	//Set status if needed
	if this.status == -1 {
		this.status = http.StatusOK
	}
	//Set time and Last-Modified if needed
	if lastUpdateStr := this.headers.Get("Last-Modified"); len(lastUpdateStr) == 0 {
		this.lastUpdated = time.Now()
		this.headers.Set("Last-Modified", this.lastUpdated.UTC().Format(http.TimeFormat))
	} else {
		var err error
		if this.lastUpdated, err = time.Parse(http.TimeFormat, lastUpdateStr); err != nil {
			this.lastUpdated = time.Now()
			this.headers.Set("Last-Modified", this.lastUpdated.UTC().Format(http.TimeFormat))
		}
	}
	//Set caching time
	if len(this.headers.Get("Cache-Control")) == 0 {
		this.headers.Set("Cache-Control", this.defCacheHeader)
	}
	//We do not support range requests
	this.headers.Set("Accept-Ranges", "none")
}
