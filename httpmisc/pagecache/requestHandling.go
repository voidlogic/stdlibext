package pagecache

import (
	"io/ioutil"
	"net/http"
	"os"
	"time"

	"util/errs"
	"util/httpmisc"
	"util/logs"
)

const _CONTENT_SNIFF_MAX = 512

func ListenAndServe(addr string, handler http.Handler) error {
	return GetTimeoutServer(addr, handler).ListenAndServe()
}

func ListenAndServeTLS(addr string, certFile string, keyFile string, handler http.Handler) error {
	return GetTimeoutServer(addr, handler).ListenAndServeTLS(certFile, keyFile)
}

func GetTimeoutServer(addr string, handler http.Handler) *http.Server {
	//keeps people who are slow or are sending keep-alives from eating all our sockets
	const (
		HTTP_READ_TO  = time.Second * 20
		HTTP_WRITE_TO = time.Second * 15
	)
	return &http.Server{
		Addr:         addr,
		Handler:      handler,
		ReadTimeout:  HTTP_READ_TO,
		WriteTimeout: HTTP_WRITE_TO,
	}
}

//Sets up all folders in contentDir to be served via HTTP at /directoryName
func SetupStaticHandlers(contentDir string, shareRoot bool, pageCache *PageCache) error {
	addedc := 0
	if shareRoot { //just share the root
		rootDir, err := os.Open(contentDir)
		if err != nil {
			return errs.Append(err, "Could not open content directory: %s", contentDir)
		}
		fileInfo, err := rootDir.Stat()
		if err != nil {
			return errs.Append(err, "Could not open content directory: %s", contentDir)
		} else if !fileInfo.IsDir() {
			return errs.New("Static content directory: %s, is not a directory", contentDir)
		}
		logs.LogInfo("Sharing local directory: %s, as HTTP directory: /.", contentDir)
		handler := http.StripPrefix("/", http.FileServer(http.Dir(contentDir)))
		if pageCache != nil {
			handler = wrapCacheHandler(pageCache, handler)
		} else {
			handler = httpmisc.WrapGzipHandler(handler)
		}
		http.Handle("/", handler)
		addedc++
	} else { //don't share the root, share sub dirs
		dirEntries, err := ioutil.ReadDir(contentDir)
		if err != nil {
			return errs.Append(err, "Could not read content directory: %s", contentDir)
		}
		for _, fileInfo := range dirEntries {
			if fileInfo.IsDir() {
				addHandler(fileInfo, contentDir, pageCache)
				addedc++
			}
		}
	}
	if addedc < 1 {
		logs.LogWarning("No static content files were found to serve; Possible misconfiguration!")
	}
	return nil
}

func addHandler(e os.FileInfo, contentDir string, pageCache *PageCache) {
	virtPath, logPath := "/"+e.Name()+"/", contentDir+"/"+e.Name()+"/"
	logs.LogInfo("Sharing local directory: %s, as HTTP directory: %s.", logPath, virtPath)
	handler := http.StripPrefix(virtPath, http.FileServer(http.Dir(logPath)))
	if pageCache != nil {
		handler = wrapCacheHandler(pageCache, handler)
	} else {
		handler = httpmisc.WrapGzipHandler(handler)
	}
	http.Handle(virtPath, handler)
}

func wrapCacheHandler(pageCache *PageCache, handler http.Handler) http.Handler {
	return http.HandlerFunc(func(writer http.ResponseWriter, req *http.Request) {
		pageCache.HandleRequest(handler, req, writer)
	})
}
