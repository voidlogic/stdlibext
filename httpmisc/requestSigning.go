package httpmisc

import (
	"crypto/sha1"
	"encoding/base64"
	//"fmt"
	"net"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"sync"
	"time"

	"util/errs"
	"util/logs"
	"util/mathmisc"
)

type SigVerifier struct {
	verifySigOn, useDNS bool
	allowedHosts        map[string]bool
	allowedHostsLock    sync.RWMutex
	presharedSecret     string
	maxTimeDiffSecs     int64
}

//SigVerifier constructor
func NewSigVerifier(verifySigOn, useDNS bool, allowedHosts []string, presharedSecret string, maxTimeSkew time.Duration) (*SigVerifier, error) {
	//Issue errors and warnings
	if secretc := len(presharedSecret); secretc < 1 {
		return nil, errs.New("Empty pre-shared secret key provided. Terminating!")
	} else if secretc < 24 {
		logs.LogWarning("Pre-shared secret key provided is too short! This should greater than 24 characters in production!")
	}
	if !verifySigOn {
		logs.LogWarning("Validation of security signatures is disabled. This should be enabled if in production!")
	}
	if len(allowedHosts) == 0 {
		logs.LogWarning("Allowed hosts list is empty, no host will be able to submit requests!")
	}
	//Set things up
	this := &SigVerifier{
		verifySigOn:     verifySigOn,
		useDNS:          useDNS,
		allowedHosts:    make(map[string]bool, len(allowedHosts)),
		presharedSecret: presharedSecret,
		maxTimeDiffSecs: int64(maxTimeSkew.Seconds()),
	}
	//Add all hosts to ACL map
	for _, host := range allowedHosts {
		this.allowedHosts[host] = true
		if useDNS {
			addrs, err := net.LookupIP(host)
			if err != nil {
				logs.LogWarningExp(err, "Host: %s, was listed on the allowed hosts list, but could not be resolved", host)
				continue
			}
			for _, addr := range addrs {
				this.allowedHosts[addr.String()] = true
			}
		}
	}
	return this, nil
}

//Verifies that the signature of a request is valid.
//A valid request can fail if the time skew is to great.
func (this *SigVerifier) VerifySignature(req *http.Request) error {
	err := this.validRemoteHost(req.RemoteAddr)
	if err == nil && this.verifySigOn {
		err = this.verifySignature(req.URL.Query(), req.URL.RawQuery)
	}
	if err != nil {
		return errs.Append(err, "Request failed security verification")
	}
	return nil
}

//Verifies that the hostname provided is allowed
func (this *SigVerifier) validRemoteHost(hostname string) error {
	host, _, err := net.SplitHostPort(hostname)
	if err != nil {
		return errs.Append(err, "Could not split host and port")
	}
	this.allowedHostsLock.RLock()
	this.allowedHostsLock.RUnlock()
	if !this.allowedHosts[host] {
		if this.useDNS {
			addrs, err := net.LookupIP(host)
			if err != nil {
				return errs.Append(err, "Host: %s, could not be resolved for ACL check", hostname)
			}
			this.allowedHostsLock.RLock()
			defer this.allowedHostsLock.RUnlock()
			for _, addr := range addrs {
				if this.allowedHosts[addr.String()] {
					go func() {
						this.allowedHostsLock.Lock()
						this.allowedHosts[host] = true
						this.allowedHostsLock.Unlock()
					}()
					return nil
				}
			}
		}
		return errs.New("Remote host is not on the approved ACL")
	}
	return nil
}

//Verifies that the request signature (and timestamp) are valid
//Request query parameters:
// sgt = The UNIX epoch seconds at submission in hex (base16)
// sg  = SHA1(shared_secret + SHA1(shared_secret + query_string_minus_sig))
// where the hashing in sg are base64 (URL varient) encoded
func (this *SigVerifier) verifySignature(params url.Values, rawQuery string) error {
	const (
		timeParam = "sgt"
		sigParam  = "sg"
	)
	//Check time delta
	querySecs, err := strconv.ParseInt(params.Get(timeParam), 16, 64)
	if err != nil {
		return errs.Append(err, "Could not parse provided timestamp")
	} else if querySecs < 0 {
		return errs.New("The timestamp format is invalid; timestamp must be a positive integer")
	} else if diff := mathmisc.AbsInt64(time.Now().Unix() - querySecs); diff > this.maxTimeDiffSecs {
		return errs.New("The maximum difference between the current time and the request timestamp has been exceeded (delta: %s)", (time.Duration(diff) * time.Second).String())
	}
	//Hash (This code can easily use byte.Buffer, but it becomes much less readable. Change if needed in future)
	providedSig, sigPos := params.Get(sigParam), strings.Index(rawQuery, "&"+sigParam+"=")
	if len(providedSig) == 0 || sigPos == -1 {
		return errs.New("Query is missing a security signature parameter")
	}
	expectedSig := getSHA1InBase64(this.presharedSecret + getSHA1InBase64(this.presharedSecret+rawQuery[0:sigPos]))
	if expectedSig != providedSig {
		return errs.New("Signature is invalid; provided and calculated signatures do not match.")
	}
	return nil
}

//Returns the base64 SHA1 hash of string
func getSHA1InBase64(str string) string {
	sha1Func := sha1.New()
	sha1Func.Write([]byte(str))
	return base64.URLEncoding.EncodeToString(sha1Func.Sum(nil))
}

var httpReqSignFailMsg = []byte("401 - Unauthorized: Request failed securty verification!")

const logReqSignFailMsg = "Request failed securty verification"

func WrapSignedReqHandler(verifier *SigVerifier, handler http.Handler) http.HandlerFunc {
	return http.HandlerFunc(func(writer http.ResponseWriter, req *http.Request) {
		if err := verifier.VerifySignature(req); err != nil {
			logs.LogWarningExp(err, logReqSignFailMsg)
			writer.WriteHeader(http.StatusUnauthorized)
			writer.Write(httpReqSignFailMsg)
			return
		}
		handler.ServeHTTP(writer, req)
	})
}

func WrapSignedReqHandlerFunc(verifier *SigVerifier, handlerFunc http.HandlerFunc) http.HandlerFunc {
	return http.HandlerFunc(func(writer http.ResponseWriter, req *http.Request) {
		if err := verifier.VerifySignature(req); err != nil {
			logs.LogInfo(logReqSignFailMsg, err)
			writer.WriteHeader(http.StatusUnauthorized)
			writer.Write(httpReqSignFailMsg)
			return
		}
		handlerFunc(writer, req)
	})
}
