package httpmisc

import (
	"net/http"
	"net/url"
)

func NoProxyAllowed(request *http.Request) (*url.URL, error) {
	return nil, nil
}

func CloneRequest(req *http.Request, removeHopByHop bool) *http.Request {
	clone := new(http.Request)
	*clone = *req
	clone.URL, clone.Header = new(url.URL), CloneHeader(req.Header)
	*clone.URL = *req.URL
	return clone
}

func CloneHeader(src http.Header) http.Header {
	dst := make(http.Header, len(src))
	var key, entry string
	var val []string
	for key, val = range src {
		for _, entry = range val {
			dst.Add(key, entry)
		}
	}
	return dst
}
