package jsonconfig

import (
	"bytes"
	"encoding/json"
	"io"
	"os"
	"strings"
)

func LoadFromFile(fname string, confStruct interface{}) error {
	fin, err := os.Open(fname)
	if err != nil {
		return err
	}
	defer fin.Close()
	return LoadFromReader(fin, &confStruct)
}

func LoadFromString(str string, conf interface{}) error {
	return LoadFromReader(strings.NewReader(str), &conf)
}

func LoadFromBytes(str []byte, conf interface{}) error {
	return LoadFromReader(bytes.NewReader(str), &conf)
}

func LoadFromReader(reader io.Reader, conf interface{}) error {
	return json.NewDecoder(reader).Decode(&conf)
}
