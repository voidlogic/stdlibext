package logs

import (
	"errors"
	"io"
	"log"

	"util/events"
)

//TODO: Add e-mail notification options

type LogLevel uint

const (
	DEBUG = iota
	INFO
	WARNING
	ERROR
	FATAL
	invalid_LEVEL
	DEFAULT_LEVEL = INFO
	_DETAILS      = ";\n\tDetails: "
)

var (
	logLevel  LogLevel = DEFAULT_LEVEL
	logPrefix string
)

func SetOutput(writer io.Writer) {
	log.SetOutput(writer)
}

func SetLogLevel(level LogLevel) error {
	if level >= DEBUG && level < invalid_LEVEL {
		logLevel = level
		return nil
	}
	return errors.New("Could not set logging level. Provided level is not valid.")
}

func SetLogPrefix(prefix string) {
	logPrefix = prefix
}

func LogDebug(errMsg string, args ...interface{}) {
	if logLevel <= DEBUG {
		print("DEBUG: "+errMsg, args)
	}
}

func LogInfo(errMsg string, args ...interface{}) {
	if logLevel <= INFO {
		print("INFO: "+errMsg, args)
	}
}

func LogWarning(errMsg string, args ...interface{}) {
	if logLevel <= WARNING {
		print("WARNING: "+errMsg, args)
	}
}

func LogError(errMsg string, args ...interface{}) {
	if logLevel <= ERROR {
		print("ERROR: "+errMsg, args)
	}
}

func LogFatal(errMsg string, args ...interface{}) {
	if logLevel <= FATAL {
		print("FATAL ERROR: "+errMsg, args)
	}
	events.RunShutdownHandler(nil)
}

func LogWarningExp(err error, errMsg string, args ...interface{}) {
	if err != nil && logLevel <= WARNING {
		print("WARNING: "+errMsg+_DETAILS+err.Error(), args)
	}
}

func LogErrorExp(err error, errMsg string, args ...interface{}) {
	if err != nil && logLevel <= ERROR {
		print("ERROR: "+errMsg+_DETAILS+err.Error(), args)
	}
}

func LogFatalExp(err error, errMsg string, args ...interface{}) {
	if err != nil {
		if logLevel <= FATAL {
			print("FATAL ERROR: "+errMsg+_DETAILS+err.Error(), args)
		}
		events.RunShutdownHandler(err)
	}
}

func print(format string, args []interface{}) {
	if len(logPrefix) > 0 {
		format = logPrefix + format
	}
	if len(args) == 0 {
		log.Println(format)
	} else {
		log.Printf(format, args...)
	}
}
