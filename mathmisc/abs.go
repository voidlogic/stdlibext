package mathmisc

//Math does not contain integer abs functions-

func Abs(x int) int {
	switch {
	case x < 0:
		return -x
	case x == 0: //abs(-0)
		return 0
	}
	return x
}

func AbsInt64(x int64) int64 {
	switch {
	case x < 0:
		return -x
	case x == 0: //abs(-0)
		return 0
	}
	return x
}

func AbsInt32(x int32) int32 {
	switch {
	case x < 0:
		return -x
	case x == 0: //abs(-0)
		return 0
	}
	return x
}
