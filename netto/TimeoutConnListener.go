package netto

import (
	"net"
	"time"
)

var _ net.Listener = &TimeoutConnListener{}

type TimeoutConnListener struct {
	net.Listener
	ioTimeout time.Duration
}

func NewTimeoutConnListener(network, address string, ioTimeout time.Duration) (*TimeoutConnListener, error) {
	listener, err := net.Listen(network, address)
	if err != nil {
		return nil, err
	}
	return &TimeoutConnListener{
		Listener:  listener,
		ioTimeout: ioTimeout,
	}, nil
}

func (this *TimeoutConnListener) Accept() (conn net.Conn, err error) {
	if conn, err = this.Listener.Accept(); err != nil {
		return nil, err
	}
	return NewTimeoutConn(conn, this.ioTimeout)
}
