package pid

import (
	"io/ioutil"
	"os"
	"strconv"
	"syscall"

	"util/errs"
	"util/logs"
)

func CheckPIDConflict(pidFilename string) error {
	logs.LogInfo("Checking for another already running instance (using PID file \"%s\")...", pidFilename)
	pidFileContent, err := ioutil.ReadFile(pidFilename)
	if err != nil {
		if pathErr, isPathErr := err.(*os.PathError); pathErr.Err == syscall.ENOENT && isPathErr {
			logs.LogInfo("OK, no PID file already exists at %s.", pidFilename)
			return nil
		}
		return errs.Append(err, "Could not open PID file: %s, please manually remove", pidFilename)
	}
	oldPIDString := string(pidFileContent)
	oldPID, err := strconv.Atoi(oldPIDString)
	if err != nil {
		return errs.Append(err, "PID file: %s, contained value: %s, which could not be parsed", pidFilename, oldPIDString)
	}
	logs.LogInfo("Found PID file with with PID: " + oldPIDString + "; checking if process is running...")
	if err = syscall.Kill(oldPID, 0); err != nil {
		logs.LogInfo("Process was not running, removing PID file.")
		if err = os.Remove(pidFilename); err != nil {
			return errs.Append(err, "Could not remove old PID file: %s; please manually remove", pidFilename)
		}
		return nil
	}
	return errs.New("A previous instance of this process is running with ID %s; please shutdown this process before running.", oldPIDString)
}

func CreatePID(pidFilename string) error {
	PID := os.Getpid()
	logs.LogInfo("Writing process ID %d to %s...", PID, pidFilename)
	if err := ioutil.WriteFile(pidFilename, []byte(strconv.Itoa(PID)), 0600); err != nil {
		return errs.Append(err, "Could not write process ID to file: \"%s\"", pidFilename)
	}
	return nil
}

func RemovePID(pidFilename string) error {
	return os.Remove(pidFilename)
}
