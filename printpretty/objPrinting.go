package printpretty

import (
	"bytes"
	"fmt"
	"io"
	"reflect"
	"text/tabwriter"
)

const _DELIM = ' '

func Print(object interface{}) string {
	buffer := new(bytes.Buffer)
	tabWriter := tabwriter.NewWriter(buffer, 4, 4, 1, _DELIM, 0)
	appendObject(object, "", tabWriter)
	tabWriter.Flush()
	return buffer.String()
}

func appendObject(object interface{}, tabs string, writer io.Writer) {
	if _, ok := object.(fmt.Stringer); ok {
		fmt.Fprintf(writer, "%q", object)
		return
	}
	switch reflect.Indirect(reflect.ValueOf(object)).Kind() {
	case reflect.Struct:
		appendStruct(object, tabs, writer)
	case reflect.Array, reflect.Slice:
		appendSlice(object, tabs, writer)
	case reflect.Map:
		appendMap(object, tabs, writer)
	case reflect.String:
		fmt.Fprintf(writer, "%q", object)
	default:
		fmt.Fprintf(writer, "%v", object)
	}
}

func appendMap(object interface{}, tabs string, writer io.Writer) {
	fmt.Fprintf(writer, "{\n")
	val := reflect.Indirect(reflect.ValueOf(object))
	for _, k := range val.MapKeys() {
		fmt.Fprint(writer, tabs, "\t", k.String(), ":\t")
		appendObject(val.MapIndex(k).Interface(), tabs+"\t", writer)
		fmt.Fprintln(writer, ",")
	}
	fmt.Fprint(writer, tabs, "}")
}

func appendStruct(object interface{}, tabs string, writer io.Writer) {
	fmt.Fprintf(writer, "%T (\n", object)
	val := reflect.Indirect(reflect.ValueOf(object))
	t := val.Type()
	fieldCount := t.NumField()
	for i := 0; i < fieldCount; i++ {
		if t.Field(i).PkgPath == "" {
			fmt.Fprint(writer, tabs, "\t", t.Field(i).Name, ":\t")
			appendObject(val.Field(i).Interface(), tabs+"\t\t", writer)
			fmt.Fprintln(writer, ",")
		}
	}
	fmt.Fprint(writer, tabs, ")")
}

func appendSlice(object interface{}, tabs string, writer io.Writer) {
	val := reflect.Indirect(reflect.ValueOf(object))
	length := val.Len()
	if length == 0 {
		fmt.Fprint(writer, "[]")
		return
	}
	fmt.Fprintf(writer, "[\n")
	for i := 0; i < length; i++ {
		fmt.Fprint(writer, tabs, "\t")
		appendObject(val.Index(i).Interface(), tabs+"\t", writer)
		fmt.Fprintln(writer, ",")
	}
	fmt.Fprint(writer, tabs, "]")
}
