package sema

//A counting semaphore implementation

import (
	"sync"
)

type Semaphore struct {
	count  int
	lock   *sync.Mutex
	wakeup *sync.Cond
}

func NewSemaphore(count int) *Semaphore {
	lock := new(sync.Mutex)
	return &Semaphore{
		count:  count,
		lock:   lock,
		wakeup: sync.NewCond(lock),
	}
}

func (s *Semaphore) P() {
	s.Wait(1)
}

func (s *Semaphore) Wait(units uint) {
	s.lock.Lock()
	defer s.lock.Unlock()
	s.count -= int(units)
	for s.count < 0 {
		s.wakeup.Wait()
	}
}

func (s *Semaphore) V() {
	s.Signal(1)
}

func (s *Semaphore) Signal(units uint) {
	s.lock.Lock()
	defer s.lock.Unlock()
	wakeOthers := s.count < 0
	s.count += int(units)
	if wakeOthers {
		s.wakeup.Signal()
	}
}
